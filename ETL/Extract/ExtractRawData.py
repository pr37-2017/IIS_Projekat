import sys,os
import pandas as pd

'''
Functions for reading from multiple excel sheets and exporting read dataframes
to a database
path: "../../RawData/training_data.xlsx"
'''
class ExtractRawData:
    def __init__(self,path,sheet_names):
        self.raw_data_excel = read_raw_data_excel_sheet(path,sheet_names)
        


'''
Read all sheets from an excel and return a list of dataframes
'''
def read_raw_data_excel_sheets(path,sheet_names):
    sheets = []
    for sheet_name in sheet_names:
        sheets.append(pd.read_excel(path,sheet_name))
    return sheets

'''
Read one sheet from an excel workbook and return a single dataframe
'''
def read_raw_data_excel_sheet(path,sheet_name):
    return pd.read_excel(path,sheet_name)


