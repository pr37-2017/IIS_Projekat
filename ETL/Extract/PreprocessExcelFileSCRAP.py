import xlrd
#SHOULD USE OPENPYXL, this lib doesn't support xlsx format.
from xlrd.sheet import ctype_text

class PreprocessExcelFile:
    def __init__(self,path):
        self.sheet_names = self.read_excel_sheet_names(path)
        self.wather_raw = xlrd.open_workbook(path).sheet_by_name("weather")
        self.sun_raw_2016 = xlrd.open_workbook(path).sheet_by_name("sunrise_sunset_2016")
        self.sun_raw_2017 = xlrd.open_workbook(path).sheet_by_name("sunrise_sunset_2017")
        self.sun_raw_2018 = xlrd.open_workbook(path).sheet_by_name("sunrise_sunset_2018")
        self.sun_raw_2019 = xlrd.open_workbook(path).sheet_by_name("sunrise_sunset_2019")
        self.wather_legend_raw = xlrd.open_workbook(path).sheet_by_name("weather_legend")
        self.load_raw = xlrd.open_workbook(path).sheet_by_name("load")

    def read_excel_sheet_names(self,path):
        workbook = xlrd.open_workbook(path,on_demand=True)
        return workbook.sheet_names()

    def process_weather(self):
        column_names = []
        row = self.wather_raw.row(0)
        for idx, cell_obj in enumerate(row):
            #cell_type_str = ctype_text.get(cell_obj.ctype, 'unknown type')
            #print('(%s) %s %s' % (idx, cell_type_str, cell_obj.value))
            if not cell_obj.value:
                continue
            column_names.append(cell_obj.value)
            print(cell_obj.value)