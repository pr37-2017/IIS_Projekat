import pandas as pd
import numpy as np

pd.options.mode.chained_assignment = None  # default='warn'

WeatherDF = pd.read_excel("../../RawData/training_data.xlsx", sheet_name='weather')  # sheetname is optional
LoadDF = pd.read_excel("../../RawData/training_data.xlsx", sheet_name='load')  # sheetname is optional
#df.to_csv('weather.csv', index=False)  # index=False prevents pandas to write row index


def process_sunrise_sunset(year):
    df2 = pd.read_excel("../../RawData/training_data.xlsx", sheet_name='sunrise_sunset_'+year)  # sheetname is optional

    df3 = df2.loc[~df2['January '+year].isin({'January '+year,'May '+year,'September '+year})]

    #January 2016,Unnamed: 1,Unnamed: 2,February 2016,Unnamed: 4,Unnamed: 5,March 2016,Unnamed: 7,Unnamed: 8,April 2016,Unnamed: 10,Unnamed: 11
    df3.rename(columns = {'January '+year:'Date1'}, inplace = True)
    df3.rename(columns = {'Unnamed: 1':'Sunrise1'}, inplace = True)
    df3.rename(columns = {'Unnamed: 2':'Sunset1'}, inplace = True)
    df3.rename(columns = {'February '+year:'Date2'}, inplace = True)
    df3.rename(columns = {'Unnamed: 4':'Sunrise2'}, inplace = True)
    df3.rename(columns = {'Unnamed: 5':'Sunset2'}, inplace = True)
    df3.rename(columns = {'March '+year:'Date3'}, inplace = True)
    df3.rename(columns = {'Unnamed: 7':'Sunrise3'}, inplace = True)
    df3.rename(columns = {'Unnamed: 8':'Sunset3'}, inplace = True)
    df3.rename(columns = {'April '+year:'Date4'}, inplace = True)
    df3.rename(columns = {'Unnamed: 10':'Sunrise4'}, inplace = True)
    df3.rename(columns = {'Unnamed: 11':'Sunset4'}, inplace = True)

    df3 = df3.iloc[1: , :] #remove the remaining column names row
    df3.reset_index()

    #print(df3.to_string())
    #df3.to_csv('srss.csv', index=False)  # index=False prevents pandas to write row index


    #Finding indexes for slicing
    #idx_for_slicing = df.index[df['Date1'] == "Date"]
    idx_for_slicing = list(np.where(df3['Date1'] == "Date")[0])
    idx_for_slicing.append(df3.index[-1])
    idx_for_slicing = [x+1 for x in idx_for_slicing]
    print(idx_for_slicing)

    #separating each month and adding to a list
    months = []
    last_idx = 0
    for i in range(len(idx_for_slicing)):
        for x in range(0,10,3):
            months.append(df3.iloc[last_idx:idx_for_slicing[i] , x:(x+3)])
        last_idx = idx_for_slicing[i]


    #Dates Formatting and appenging to final DF
    SS201xDF = pd.DataFrame()
    for idx, m in enumerate(months):
        m = m.dropna()
        m.iloc[:, 0] = m.iloc[:, 0].str.extract('(\d+)')
        m.iloc[:, 0] = m.iloc[:, 0].astype(str)
        m.iloc[:, 0] += "." + str((idx+1)) + "." + year +"."
        m = m.rename(columns={m.columns[0]: 'Date'})
        m = m.rename(columns={m.columns[1]: 'Sunrise'})
        m = m.rename(columns={m.columns[2]: 'Sunset'})
        SS201xDF = SS201xDF.append(m,ignore_index=True)

    SS201xDF = SS201xDF[SS201xDF["Date"].str.contains("nan")==False] #remove remaining nan
    SS201xDF["Date"] = SS201xDF["Date"].str.replace(r'^(0+)', '') #remove leading zeroes
    SS201xDF["Date"] = pd.to_datetime(SS201xDF['Date']) #remove leading zeroes
    SS201xDF["DayName"] = SS201xDF["Date"].dt.day_name()
    #print(SS201xDF.to_string())
    return SS201xDF


years_to_process = ["2016", "2017", "2018", "2019"]
SunriseSunsetDF = pd.DataFrame()
for year in years_to_process:
    SunriseSunsetDF = SunriseSunsetDF.append(process_sunrise_sunset(year),ignore_index=True)

#print(SunriseSunsetDF.to_string())

#TODO: Has a lot of NaNs
WeatherDF = WeatherDF.drop({'Unnamed: 29','Unnamed: 30'}, axis=1)

#Separate date and time into two columns
WeatherDF = WeatherDF.rename(columns={WeatherDF.columns[0]: 'Date'})
WeatherDF['Date'] = pd.to_datetime(WeatherDF['Date'])
WeatherDF['Time'] = WeatherDF['Date'].dt.time
WeatherDF['Date'] = WeatherDF['Date'].dt.date

#print(WeatherDF)

SunriseSunsetDF.to_csv('../../PreprocessedData/SunriseSunsedPreprocessed.csv', index=False)
WeatherDF.to_csv('../../PreprocessedData/WeatherDF.csv', index=False)
LoadDF.to_csv('../../PreprocessedData/LoadDF.csv', index=False)

'''
for rowIndex, row in df2.iterrows(): #iterate over rows
    for columnIndex, value in row.items():
        if value
        '''


'''
from openpyxl import Workbook
from openpyxl.worksheet.datavalidation import DataValidation
from openpyxl import load_workbook
import csv

wb = load_workbook(filename = "../../RawData/training_data.xlsx")
#sheet_ranges = wb['sheet name']
#print(sheet_ranges['D18'].value)

sh = wb.active
with open('test.csv', 'wb') as f:  # open('test.csv', 'w', newline="") for python 3
    c = csv.writer(f)
    for r in sh.rows:
        c.writerow([cell.value for cell in r])
        
'''
